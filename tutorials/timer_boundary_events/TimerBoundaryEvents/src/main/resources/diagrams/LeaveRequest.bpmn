<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:activiti="http://activiti.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.activiti.org/LeaveRequestProccess">
  <process id="LeaveRequestProccess" name="Leave Request" isExecutable="true">
    <startEvent id="startevent1" name="Start" activiti:initiator="admin">
      <extensionElements>
        <activiti:formProperty id="numberOfDays" name="Number of days" type="long" required="true"></activiti:formProperty>
        <activiti:formProperty id="startDate" name="First day of holiday (dd-MM-yyyy)" type="string" datePattern="dd-MM-yyyy hh:mm" required="true"></activiti:formProperty>
        <activiti:formProperty id="vacationReason" name="Reason" type="string" required="true"></activiti:formProperty>
        <activiti:formProperty id="employeeName" name="Name of the employee" type="string" required="true"></activiti:formProperty>
      </extensionElements>
    </startEvent>
    <sequenceFlow id="flow1" sourceRef="startevent1" targetRef="usertask1"></sequenceFlow>
    <userTask id="usertask1" name="Review leave request" activiti:assignee="manager">
      <extensionElements>
        <activiti:formProperty id="vacationApproved" name="Do you approve this request?" type="enum" required="true">
          <activiti:value id="true" name="Approve"></activiti:value>
          <activiti:value id="false" name="Reject"></activiti:value>
        </activiti:formProperty>
      </extensionElements>
    </userTask>
    <endEvent id="endevent1" name="End"></endEvent>
    <exclusiveGateway id="exclusivegateway1" name="Request approved?"></exclusiveGateway>
    <sequenceFlow id="flow2" sourceRef="usertask1" targetRef="exclusivegateway1"></sequenceFlow>
    <sequenceFlow id="flow3" sourceRef="exclusivegateway1" targetRef="mailtask1">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${vacationApproved == 'true'}]]></conditionExpression>
    </sequenceFlow>
    <userTask id="usertask2" name="Adjust vacation request" activiti:assignee="admin">
      <extensionElements>
        <activiti:formProperty id="numberOfDays" name="Number of days" type="long" required="true"></activiti:formProperty>
        <activiti:formProperty id="startDate" name="First day of holiday (dd-MM-yyyy)" type="string" datePattern="dd-MM-yyyy hh:mm " required="true"></activiti:formProperty>
        <activiti:formProperty id="vacationReason" name="Reason" type="string" required="true"></activiti:formProperty>
        <activiti:formProperty id="employeeName" name="Name of employee" type="string" required="true"></activiti:formProperty>
        <activiti:formProperty id="resendRequest" name="Resend vacation request to manager?" type="enum" required="true">
          <activiti:value id="true" name="Yes"></activiti:value>
          <activiti:value id="false" name="No"></activiti:value>
        </activiti:formProperty>
      </extensionElements>
    </userTask>
    <sequenceFlow id="flow4" sourceRef="exclusivegateway1" targetRef="usertask2">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${vacationApproved == 'false'}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow5" sourceRef="usertask2" targetRef="usertask1"></sequenceFlow>
    <serviceTask id="mailtask1" name="Send confirmation email" activiti:type="mail">
      <extensionElements>
        <activiti:field name="to">
          <activiti:string><![CDATA[pedro@pedropiloto.com]]></activiti:string>
        </activiti:field>
        <activiti:field name="from">
          <activiti:string><![CDATA[insis.spammer@gmail.com]]></activiti:string>
        </activiti:field>
        <activiti:field name="subject">
          <activiti:string><![CDATA[Leave Approved!]]></activiti:string>
        </activiti:field>
        <activiti:field name="text">
          <activiti:expression><![CDATA[Hi ${employeeName}, your leave request is approved.]]></activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow6" sourceRef="mailtask1" targetRef="endevent1"></sequenceFlow>
    <boundaryEvent id="boundarytimer1" name="Timer" attachedToRef="usertask1" cancelActivity="false">
      <timerEventDefinition>
        <timeDuration>PT60S</timeDuration>
      </timerEventDefinition>
    </boundaryEvent>
    <serviceTask id="mailtask2" name="Mail Task" activiti:type="mail">
      <extensionElements>
        <activiti:field name="from">
          <activiti:string><![CDATA[insis.spammer@gmail.com]]></activiti:string>
        </activiti:field>
        <activiti:field name="to">
          <activiti:string><![CDATA[pedro@pedropiloto.com]]></activiti:string>
        </activiti:field>
        <activiti:field name="subject">
          <activiti:string><![CDATA[Leave Not Approved!]]></activiti:string>
        </activiti:field>
        <activiti:field name="text">
          <activiti:expression><![CDATA[Hi ${employeeName}, your leave request is not approved.]]></activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow7" sourceRef="boundarytimer1" targetRef="callactivity1"></sequenceFlow>
    <endEvent id="terminateendevent1" name="TerminateEndEvent">
      <terminateEventDefinition></terminateEventDefinition>
    </endEvent>
    <sequenceFlow id="flow8" sourceRef="mailtask2" targetRef="terminateendevent1"></sequenceFlow>
    <callActivity id="callactivity1" name="Call activity" calledElement="myProcess"></callActivity>
    <sequenceFlow id="flow9" sourceRef="callactivity1" targetRef="mailtask2"></sequenceFlow>
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_LeaveRequestProccess">
    <bpmndi:BPMNPlane bpmnElement="LeaveRequestProccess" id="BPMNPlane_LeaveRequestProccess">
      <bpmndi:BPMNShape bpmnElement="startevent1" id="BPMNShape_startevent1">
        <omgdc:Bounds height="35.0" width="35.0" x="180.0" y="200.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="usertask1" id="BPMNShape_usertask1">
        <omgdc:Bounds height="55.0" width="105.0" x="290.0" y="190.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="boundarytimer1" id="BPMNShape_boundarytimer1">
        <omgdc:Bounds height="30.0" width="30.0" x="350.0" y="169.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="endevent1" id="BPMNShape_endevent1">
        <omgdc:Bounds height="35.0" width="35.0" x="760.0" y="200.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="exclusivegateway1" id="BPMNShape_exclusivegateway1">
        <omgdc:Bounds height="40.0" width="40.0" x="440.0" y="198.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="usertask2" id="BPMNShape_usertask2">
        <omgdc:Bounds height="55.0" width="105.0" x="420.0" y="290.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="mailtask1" id="BPMNShape_mailtask1">
        <omgdc:Bounds height="55.0" width="105.0" x="580.0" y="190.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="mailtask2" id="BPMNShape_mailtask2">
        <omgdc:Bounds height="55.0" width="105.0" x="330.0" y="40.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="terminateendevent1" id="BPMNShape_terminateendevent1">
        <omgdc:Bounds height="35.0" width="35.0" x="600.0" y="50.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="callactivity1" id="BPMNShape_callactivity1">
        <omgdc:Bounds height="55.0" width="105.0" x="500.0" y="84.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge bpmnElement="flow1" id="BPMNEdge_flow1">
        <omgdi:waypoint x="215.0" y="217.0"></omgdi:waypoint>
        <omgdi:waypoint x="290.0" y="217.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow2" id="BPMNEdge_flow2">
        <omgdi:waypoint x="395.0" y="217.0"></omgdi:waypoint>
        <omgdi:waypoint x="440.0" y="218.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow3" id="BPMNEdge_flow3">
        <omgdi:waypoint x="480.0" y="218.0"></omgdi:waypoint>
        <omgdi:waypoint x="580.0" y="217.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow4" id="BPMNEdge_flow4">
        <omgdi:waypoint x="460.0" y="238.0"></omgdi:waypoint>
        <omgdi:waypoint x="472.0" y="290.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow5" id="BPMNEdge_flow5">
        <omgdi:waypoint x="472.0" y="290.0"></omgdi:waypoint>
        <omgdi:waypoint x="342.0" y="245.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow6" id="BPMNEdge_flow6">
        <omgdi:waypoint x="685.0" y="217.0"></omgdi:waypoint>
        <omgdi:waypoint x="760.0" y="217.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow7" id="BPMNEdge_flow7">
        <omgdi:waypoint x="365.0" y="169.0"></omgdi:waypoint>
        <omgdi:waypoint x="552.0" y="139.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow8" id="BPMNEdge_flow8">
        <omgdi:waypoint x="435.0" y="67.0"></omgdi:waypoint>
        <omgdi:waypoint x="600.0" y="67.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow9" id="BPMNEdge_flow9">
        <omgdi:waypoint x="500.0" y="111.0"></omgdi:waypoint>
        <omgdi:waypoint x="382.0" y="95.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>
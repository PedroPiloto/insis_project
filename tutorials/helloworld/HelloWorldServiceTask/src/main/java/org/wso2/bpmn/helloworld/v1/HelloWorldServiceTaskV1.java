package org.wso2.bpmn.helloworld.v1;

import java.util.ArrayList;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class HelloWorldServiceTaskV1 implements JavaDelegate {

	public void execute(DelegateExecution arg0) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("TO_PRINT: " + arg0.getVariable("to_print"));
		System.out.println("variable names: " + arg0.getVariableNames());
		System.out.println("variable names local: " + arg0.getVariableNamesLocal());
		System.out.println("Hello World");
	}

}
package org.wso2.bpmn.tasklistener;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

public class HowVoteTaskListenerV1 implements  TaskListener{

	public void notify(DelegateTask arg0) {
		DelegateExecution execution = arg0.getExecution();
		
		String defaultVotes = (String) execution.getVariable("options_str");
		System.out.println(defaultVotes);
		
		

		//ArrayList<Vote> initialVotes =  new ArrayList<Vote>();
		//initialVotes.add(new Vote("0","cozinhar",0));
		//initialVotes.add(new Vote("1","encomendar",0));
		//initialVotes.add(new Vote("2","comer fora",0));		
		
		String vote = (String)execution.getVariable("how");
		System.out.println("vote: " + vote);
		
		Object votes = execution.getVariable("votes");
		System.out.println("votes: " + votes);
		
		ArrayList<Vote> initialVotes = stringToArrayList(defaultVotes);
		
		if(initialVotes == null) {
			System.out.println("No Options to vote");
			execution.setVariable("votes", "");
			return;
		}
		
		if(votes != null && votes instanceof String) {
			System.out.println("preparing to update votes");
			//Convert to Array List
			ArrayList<Vote> voteArrayList = stringToArrayList((String)votes);
					System.out.println("After converting the votes string to array list, the size of it is: " + voteArrayList.size());
			ArrayList<Vote> updatedVotes = findAndUpdateVote(voteArrayList, vote);
			System.out.println("Updated votes");
			
			//Convert To String
			System.out.println("Converting votes array list to string");
			String votesString = arrayListToString(voteArrayList);
			System.out.println("Votes String: " + votesString );
			execution.setVariable("votes", votesString);
		}else {
			//String defaultVotes =  arrayListToString(initialVotes);
			System.out.println("no votes, probably this is the first one: " + defaultVotes);
			
			//Convert to Array List
			ArrayList<Vote> voteArrayList = stringToArrayList(defaultVotes);
					System.out.print("After converting the votes string to array list, the size of it is: " + voteArrayList.size());
			ArrayList<Vote> updatedVotes = findAndUpdateVote(voteArrayList, vote);
			System.out.println("Updated votes");
			//Convert To String
			System.out.println("Converting votes array list to string");
			String votesString = arrayListToString(voteArrayList);
			System.out.println("Votes String: " + votesString );
			execution.setVariable("votes", votesString);
		}
	}
	
	public ArrayList<Vote> findAndUpdateVote(ArrayList<Vote> votes, String id) {
		
		for(Vote vote : votes) {
			if(vote.getId().equals(id)) {
				vote.setCount(vote.getCount() + 1);
				break;
			}
		}
		return votes;
	}
	
	public String arrayListToString(ArrayList<Vote> votes) {
		System.out.println("preparing to convert votes array list to string");
		String votesString = "";
		for(int i =0; i < votes.size();i++) {
			votesString += votes.get(i).toString();
			if(i!=votes.size()-1) {
				votesString += "#";
			}
		}
		System.out.println("Votes string: " + votesString);
		return votesString;
	}
	
	public ArrayList<Vote> stringToArrayList(String votes){
		ArrayList<Vote> votesArrayList = new ArrayList<Vote>();
		
		System.out.println("preparing to convert votes string to array list");
		String[] votesArray = votes.split("#");
		System.out.println("votes size: " + votesArray.length);
		String id, name;
		Integer count;
		for(int i =0; i< votesArray.length;i++) {
			String[] voteProperties = votesArray[i].split("_");
			System.out.println("votesProperties size: " + voteProperties.length);
			if(voteProperties.length != 3) {
				System.out.println("Invalid Option");
				return null;
			}
			id = voteProperties[0];
			name = voteProperties[1];
			count = Integer.parseInt(voteProperties[2]);
			System.out.println(id+"_"+name+"_"+count);
			Vote v  = new Vote(id, name, count);
			System.out.println("After creating object");
			votesArrayList.add(v);
			System.out.println("After adding to array list");
		}
		return votesArrayList;
	}
	
	class Vote{
		private String id;
		private String name;
		private Integer count = 0;
		
		public Vote(String id, String name, Integer count) {
			super();
			this.id = id;
			this.name = name;
			this.count = count;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getCount() {
			return count;
		}
		public void setCount(Integer count) {
			this.count = count;
		}
		@Override
		public String toString() {
			return id.trim() + "_" + name.trim() + "_" + count;
		}
		
		
		
	}

}

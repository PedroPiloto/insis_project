package org.apache.customtype;

import org.activiti.engine.form.AbstractFormType;

public class SelectionFormType extends AbstractFormType{

	public String getName() {
		return "selection";
	}

	@Override
	public Object convertFormValueToModelValue(String arg0) {
		Integer selection = Integer.valueOf(arg0);
		return selection;
	}

	@Override
	public String convertModelValueToFormValue(Object arg0) {
		if(arg0 == null) {
			return null;
		}
		return arg0.toString();
	}

}

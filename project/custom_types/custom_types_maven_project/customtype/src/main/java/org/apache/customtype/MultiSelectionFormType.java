package org.apache.customtype;

import org.activiti.engine.form.AbstractFormType;

public class MultiSelectionFormType extends AbstractFormType{

	public String getName() {
		return "multiSelection";
	}

	@Override
	public Object convertFormValueToModelValue(String arg0) {
		Integer multiSelection = Integer.valueOf(arg0);
		return multiSelection;
	}

	@Override
	public String convertModelValueToFormValue(Object arg0) {
		if(arg0 == null) {
			return null;
		}
		return arg0.toString();
	}

}

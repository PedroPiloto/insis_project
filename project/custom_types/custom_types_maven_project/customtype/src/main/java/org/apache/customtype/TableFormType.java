package org.apache.customtype;

import org.activiti.engine.form.AbstractFormType;

public class TableFormType extends AbstractFormType{

	public String getName() {
		return "table";
	}

	@Override
	public Object convertFormValueToModelValue(String arg0) {
		Integer table = Integer.valueOf(arg0);
		return table;
	}

	@Override
	public String convertModelValueToFormValue(Object arg0) {
		if(arg0 == null) {
			return null;
		}
		return arg0.toString();
	}

}
